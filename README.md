# How it works
![Archi](How-to.png)

# Prerequisite
- NodeJs 12.X https://nodejs.org/en/download/
- Docker https://docs.docker.com/get-docker/
- docker-compose https://docs.docker.com/compose/install/

# Before Start
> $ cp docker-compose.tpl.yml docker-compose.yml

# start
> $ docker-compose up 

- Kibana : 5601
- Grafana : 3000
- Udp elastic search: udp://localhost:12201
- Metrics on services: /metrics
- Default port on service: 9001

# Add a service :
### Add the service to the docker-compose
 <pre>
container_name:
  image: image
  restart: on-failure
  volumes:
    - ./services-config/config-file.yml:/server/config.yml
  working_dir: /server
  logging:
    driver: gelf
    options:
        gelf-address: "udp://localhost:12201"
        tag: "a-super-tag"
</pre>

### Add the configuration overwrite to the services-config directory. Values to add are retreive from config.yml in the image repository
 <pre>
  volumes:
    - ./services-config/config-file.yml:/server/config.yml
</pre>

### Add the domain name to the prometheus conf -> prometheus/config.yml
<pre>
 - targets: 
    - "container_name:9001"
</pre>

### Add the service to the intent-dispatcher config.yml
<pre>
- container_name: 
  uri: "http://container_name:9001"
  intent: "my_intent"
  secured: false
</pre>

# Create a service :
You need the mitsi_bot_lib if you want to simplify object communication, but you can do everything yourself.

Expose a server that answer POST request on / with a ResponseText object.

See cli : https://gitlab.com/mitsi_bot/cli

